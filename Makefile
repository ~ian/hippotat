# Copyright 2020-2022 Ian Jackson and contributors to Hippotat
# SPDX-License-Identifier: GPL-3.0-or-later WITH LicenseRef-Hippotat-OpenSSL-Exception
# There is NO WARRANTY.

SHELL=/bin/bash

default: all

SPHINXBUILD	?= sphinx-build

INSTALL		?= install

ifneq (,$(NAILING_CARGO))

NAILING_CARGO ?= nailing-cargo
CARGO = $(NAILING_CARGO)
BUILD_SUBDIR ?= ../Build
TARGET_DIR ?= $(BUILD_SUBDIR)/$(notdir $(PWD))/target
NAILING_CARGO_JUST_RUN ?= $(NAILING_CARGO) --just-run -q ---

else

CARGO		?= cargo
TARGET_DIR	?= target

endif # Cargo.nail

CARGO_RELEASE ?= release
TARGET_RELEASE_DIR ?= $(TARGET_DIR)/$(CARGO_RELEASE)

ifneq (debug,$(CARGO_RELEASE))
CARGO_RELEASE_ARG ?= --$(CARGO_RELEASE)
endif

rsrcs = $(shell $(foreach x,$(MAKEFILE_FIND_X),set -$x;)\
    find -H $1 \( -name Cargo.toml -o -name Cargo.lock -o -name Cargo.lock.example -o -name \*.rs \) )
stamp=@mkdir -p stamp; touch $@

TESTS=$(notdir $(wildcard test/t-*[^~]))

all:	cargo-build doc

check:	cargo-test $(addprefix stamp/,$(TESTS))

cargo-build: stamp/cargo-build
cargo-test: stamp/cargo-test

stamp/cargo-%: $(call rsrcs,.)
	$(CARGO) $* $(CARGO_RELEASE_ARG) $(CARGO_BUILD_OPTIONS)
	$(stamp)

stamp/t-%: test/t-% stamp/cargo-build $(wildcard test/*[^~])
	$(NAILING_CARGO_JUST_RUN) \
	$(abspath test/capture-log) tmp/t-$*.log \
	$(abspath test/go-with-unshare test/t-$*)
	@echo OK t-$*; touch $@

doc:	docs/html/index.html
	@echo 'Documentation can now be found here:'
	@echo '  file://$(PWD)/$<'

docs/html/index.html: docs/conf.py $(wildcard docs/*.md docs/*.rst docs/*.png)
	rm -rf docs/html
	$(SPHINXBUILD) -M html docs docs $(SPHINXOPTS)

doch=/usr/share/doc/hippotat/

install: all
	$(INSTALL) -d $(DESTDIR)/usr/{bin,sbin} $(DESTDIR)$(doch)
	$(INSTALL) -m 755 $(TARGET_RELEASE_DIR)/hippotat $(DESTDIR)/usr/bin/.
	$(INSTALL) -m 755 $(TARGET_RELEASE_DIR)/hippotatd $(DESTDIR)/usr/sbin/.
	cp -r docs/html $(DESTDIR)$(doch)
	$(INSTALL) -m 644 PROTOCOL.txt $(DESTDIR)$(doch)/

clean:
	rm -rf stamp/* doc/html

very-clean: clean
	$(CARGO) clean

#---------- docs publication ----------

PUBLISHED_BRANCH=published
PUBLISH_VERSION=unreleased
PUBLISH_USER=ianmdlvl@login.chiark.greenend.org.uk
PUBLISH_DOC_SPHINX_BASE=public-html/hippotat
PUBLISH_DOC_SPHINX_TAIL=$(PUBLISH_VERSION)/docs
PUBLISH_DOC_SPHINX=$(PUBLISH_USER):$(PUBLISH_DOC_SPHINX_BASE)/$(PUBLISH_DOC_SPHINX_TAIL)

publish: doc
	ssh $(PUBLISH_USER) 'cd $(PUBLISH_DOC_SPHINX_BASE) && mkdir -p $(PUBLISH_DOC_SPHINX_TAIL)'
	rsync -r --delete-delay docs/html/. $(PUBLISH_DOC_SPHINX)/.
	git branch -f $(PUBLISHED_BRANCH)

publish-make-current:
	ssh $(PUBLISH_USER) 'set -e; cd $(PUBLISH_DOC_SPHINX_BASE); rm -f current.tmp; ln -s $(PUBLISH_VERSION) current.tmp; mv -T current.tmp current'

.PHONY: cargo-build all doc clean
